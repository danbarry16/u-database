#include <pthread.h>
#include <signal.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * database.h A very simple single-file key-value database.
 * @author B[]
 **/
#ifndef UDATABASE_LIB
  #define UDATABASE_LIB 1.21
  #define UDATABASE_HLEN 128 / 8
  #define UDATABASE_KLEN (1 << (sizeof(uint16_t) * 8))
  #define UDATABASE_NULL "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

  struct uDB{ FILE* fp; pthread_mutex_t lock; long int dWidth; long int tableLen; long int* table; };

  /**
   * udatabase_shash() Convert a string to a hash (based on djb2).
   * @param s The NULL terminated string to be hashed.
   * @param d The target pre-allocated string of hash length for the result.
   * @param n The length of the hash to be created.
   * @return A pointer to the hashed string stored in d.
   **/
  char* udatabase_shash(char* s, char* d, int n){
    memset(d, 0, n);
    for(int x = 0; *s; ++x) *(int16_t*)d = (*(int16_t*)d << 5) ^ *(int16_t*)d + (d[x % n] ^= *s++);
    return d;
  }

  /**
   * udatabase_service() Compress empty records and rebuild lookup.
   * @param db The database description.
   * @return 0 on success, otherwise -1 on failure.
   **/
  int udatabase_service(struct uDB* db){
    long int width = UDATABASE_HLEN + db->dWidth;
    char prev[UDATABASE_HLEN];
    char entry[width];
    long int shift = 0;
    pthread_mutex_lock(&db->lock);
    memset(db->table, -1, sizeof(long int) * UDATABASE_KLEN * db->tableLen);
    rewind(db->fp);
    while(fread(entry, 1, width, db->fp) == width){
      if(memcmp(entry, UDATABASE_NULL, UDATABASE_HLEN) != 0){
        if(shift > 0){
          fseek(db->fp, -(shift + width), SEEK_CUR);
          fwrite(entry, 1, width, db->fp);
          fseek(db->fp, shift, SEEK_CUR);
        }
        if(memcmp(prev, entry, UDATABASE_HLEN) == 0) continue; // Skip if repeat hash
        memcpy(prev, entry, UDATABASE_HLEN);
        for(long int t = 0; t < 65535; ++t){
          if(t >= db->tableLen){ // Expand table if required
            db->tableLen = db->tableLen > 0 ? db->tableLen * 2 : 1;
            db->table = realloc(db->table, sizeof(long int) * UDATABASE_KLEN * db->tableLen);
            if(db->table == NULL) return -1; // NOTE: Catastrophic failure, remain locked.
            memset(&db->table[UDATABASE_KLEN * t], -1, sizeof(long int) * UDATABASE_KLEN * (db->tableLen - t));
          }
          long int toff = *(uint16_t*)entry + (UDATABASE_KLEN * t);
          if(db->table[toff] < 0){ // Insert hash into LUT if there is space
            db->table[toff] = ftell(db->fp) - (width + shift);
            break;
          }
        }
      }else{
        shift += width;
      }
    }
    return (shift > 0 ? ftruncate(fileno(db->fp), ftell(db->fp) - shift) : 0) +
           (pthread_mutex_unlock(&db->lock));
  }

  /**
   * udatabase_search() Search for an entry in the database given a key.
   * @param db The database description.
   * @param k A NULL terminated key string value to be searched for.
   * @param kHash The already hashed key, otherwise NULL.
   * @return The table offset with correct fseek() and locked, -1 not found.
   **/
  long int udatabase_search(struct uDB* db, char* k, char* kHash){
    char* hash;
    char h[UDATABASE_HLEN];
    hash = kHash != NULL ? kHash : udatabase_shash(k, h, UDATABASE_HLEN);
    long int t = -1;
    long int toff = -1;
    if(db != NULL && db->fp != NULL){
      pthread_mutex_lock(&db->lock);
      while(++t < db->tableLen){
        if(db->table[toff = *(uint16_t*)hash + (UDATABASE_KLEN * t)] >= 0){
          char dhash[UDATABASE_HLEN];
          fseek(db->fp, db->table[toff], SEEK_SET);
          if(fread(dhash, 1, UDATABASE_HLEN, db->fp) == UDATABASE_HLEN){
            if(memcmp(dhash, hash, UDATABASE_HLEN) == 0){
              fseek(db->fp, -UDATABASE_HLEN, SEEK_CUR);
              break;
            }
          }
        }
      }
    } // NOTE: We do not unlock here as fseek() position could be changed.
    return toff >= 0 && t < db->tableLen ? toff : -1;
  }

  /**
   * udatabase_get() Get the data stored at the location if it exists.
   * @param db The database description.
   * @param k A NULL terminated key string value to be searched for.
   * @return The malloc'd value associated with the key, otherwise NULL.
   **/
  char* udatabase_get(struct uDB* db, char* k){
    char hash[UDATABASE_HLEN];
    udatabase_shash(k, hash, UDATABASE_HLEN);
    char* v, *val = NULL;
    if(db != NULL && db->fp != NULL && udatabase_search(db, k, hash) >= 0){ // NOTE: Remains locked.
      long int count = 0;
      char dhash[UDATABASE_HLEN];
      while(fread(dhash, 1, UDATABASE_HLEN, db->fp) == UDATABASE_HLEN &&
            memcmp(dhash, hash, UDATABASE_HLEN) == 0){
        if((val = realloc(v = val, db->dWidth * ++count)) == NULL) free(v);
        if(val == NULL || fread(val + (db->dWidth * (count - 1)), 1, db->dWidth, db->fp) != db->dWidth) break;
      }
    }
    pthread_mutex_unlock(&db->lock); // NOTE: Unlock from search() call.
    return val;
  }

  /**
   * udatabase_remove() Remove a given record and all related entries.
   * @param db The database description.
   * @oaram k The key to be matched for the entry.
   * @param kHash The already hashed key, otherwise NULL.
   * @return The table offset and held lock, -1 if not found.
   **/
  long int udatabase_remove(struct uDB* db, char* k, char* kHash){
    char* hash;
    char h[UDATABASE_HLEN];
    hash = kHash != NULL ? kHash : udatabase_shash(k, h, UDATABASE_HLEN);
    long int toff = udatabase_search(db, k, hash); // NOTE: Remains locked.
    if(db != NULL && db->fp != NULL && memcmp(hash, UDATABASE_NULL, UDATABASE_HLEN) != 0 && toff >= 0){
      db->table[toff] = -1;
      char dhash[UDATABASE_HLEN];
      while(fread(dhash, 1, UDATABASE_HLEN, db->fp) == UDATABASE_HLEN &&
            memcmp(dhash, hash, UDATABASE_HLEN) == 0){
        fseek(db->fp, -UDATABASE_HLEN, SEEK_CUR);
        fwrite(&UDATABASE_NULL, 1, UDATABASE_HLEN, db->fp);
        fseek(db->fp, db->dWidth, SEEK_CUR);
      }
    } // NOTE: We do not unlock search() here to keep table offset clear.
    return toff;
  }

  /**
   * udatabase_put() Store value using key. No check if the key already exists.
   * @param db The database description.
   * @param k A NULL terminated key string to be used to insert a value.
   * @param v A string to be inserted into the database.
   * @param n The length of the string to be stored.
   * @return The success of the insertion, 0 if success.
   **/
  int udatabase_put(struct uDB* db, char* k, char* v, long int n){
    char hash[UDATABASE_HLEN];
    udatabase_shash(k, hash, UDATABASE_HLEN);
    long int toff = udatabase_remove(db, k, hash); // NOTE: Remains locked.
    if(db != NULL && db->fp != NULL && memcmp(hash, UDATABASE_NULL, UDATABASE_HLEN) != 0){
      fseek(db->fp, 0, SEEK_END);
      if(toff >= 0){
        db->table[toff] = ftell(db->fp);
      }else{
        long int t = -1;
        while(++t < db->tableLen && db->table[toff = *(uint16_t*)hash + (UDATABASE_KLEN * t)] >= 0);
        if(t < db->tableLen && toff >= 0){
          db->table[toff] = ftell(db->fp);
        }else{
          toff = -1;
        }
      }
      long int x = 0;
      while(x < n && v != NULL){
        fwrite(&hash, 1, UDATABASE_HLEN, db->fp);
        x += fwrite(v + x, 1, (n - x) <= db->dWidth ? (n - x) : db->dWidth, db->fp);
      }
      while(x++ % db->dWidth != 0) fputc('\0', db->fp);
    }
    pthread_mutex_unlock(&db->lock); // NOTE: Unlock from remove() call.
    return toff >= 0 ? 0 : udatabase_service(db);
  }

  /**
   * udatabase_close() Close the database and stop further operation.
   * @param db The database description.
   * @return The success of closing, keeps the database in lock when closed.
   **/
  int udatabase_close(struct uDB* db){
    if(db != NULL && db->table != NULL) pthread_mutex_lock(&db->lock); // NOTE: Permanent lock.
    return fclose(db->fp) == 0 ? ((db->fp = NULL) == NULL ? 0 : -2) : -1;
  }

  /**
   * udatabase_signal() Handle a given signal and close gracefully.
   * @param db The database description.
   * @param signum The signal number indicating
   * @param ptr The raw pointer to the database structure.
   **/
  void udatabase_signal(int signum, void* ptr){
    static struct uDB* db = NULL;
    if((db = db != NULL ? db : ptr) != NULL && signum >= SIGHUP && signum <= SIGKILL)
      udatabase_close(db); // NOTE: Remains locked.
  }

  /**
   * udatabase_init() Open a database for read/write operations.
   * @param db The database description pointer.
   * @param filename The location of the database to be operated on.
   * @param width The width of an entire record.
   **/
  void udatabase_init(struct uDB* db, char* filename, long int width){
    for(int x = SIGHUP; x < SIGKILL; ++x) signal(x, (void (*)(int))udatabase_signal);
    udatabase_signal(0, db);
    db->fp = fopen(filename, "rb+");
    pthread_mutex_init(&db->lock, NULL);
    db->dWidth = width - UDATABASE_HLEN;
    db->tableLen = 0;
    db->table = NULL;
    while(udatabase_service(db) < 0);
  }

  /**
   * udatabase_create() Create a database on disk and closes it.
   * @param filename The location of the database to be create.
   **/
  void udatabase_create(char* filename){
    if(access(filename, R_OK | W_OK) != 0){
      struct uDB db = { .fp = fopen(filename, "w"), .table = NULL };
      udatabase_close(&db); // NOTE: Remains locked.
    }
  }
#endif
