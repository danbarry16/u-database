#include "udatabase.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/**
 * main.c
 *
 * The following is an example implementation of the udatabase project. It can be
 * built using the Makefile.
 **/

/* Source: https://stackoverflow.com/a/51336144/2847743 */
int64_t currentTimeMillis(){
  struct timeval time;
  gettimeofday(&time, NULL);
  int64_t s1 = (int64_t)(time.tv_sec) * 1000;
  int64_t s2 = time.tv_usec / 1000;
  return s1 + s2;
}

int64_t testPut(struct uDB* db, int width, int num){
  srand(0);
  char* k = malloc(32);
  char* test = malloc((width * 4) + num);
  for(int x = 0; x < (width * 4) + num; x++) test[x] = (char)rand();
  int64_t putStart = currentTimeMillis();
  for(int x = 0; x < num; x++){
    char* v = test + x;
    sprintf(k, "%x", x);
    int n = width + (x % (width * 3));
    if(udatabase_put(db, k, v, n) < 0){
      fprintf(stderr, "Failed to insert key=%s\n", k);
    }
  }
  return currentTimeMillis() - putStart;
}

int64_t checkPut(struct uDB* db, int width, int num){
  srand(0);
  char* k = malloc(32);
  char* test = malloc((width * 4) + num);
  for(int x = 0; x < (width * 4) + num; x++) test[x] = (char)rand();
  int64_t putStart = currentTimeMillis();
  for(int x = 0; x < num; x++){
    char* v = test + x;
    sprintf(k, "%x", x);
    int n = width + (x % (width * 3));
    char* val = udatabase_get(db, k);
    if(val != NULL){
      if(memcmp(val, v, n) != 0){
        fprintf(stderr, "For x=%i, key=%s, reading entry of length=%i\n", x, k, n);
        break;
      }
      free(val);
    }else{
      fprintf(stderr, "For x=%i, key=%s, reading entry of length=%i\n", x, k, n);
      fprintf(stderr, "Failed to read back value for x=%i\n", x);
    }
  }
  return currentTimeMillis() - putStart;
}

int64_t delPut(struct uDB* db, int width, int num){
  srand(0);
  char* k = malloc(32);
  char* test = malloc((width * 4) + num);
  for(int x = 0; x < (width * 4) + num; x++) test[x] = (char)rand();
  int64_t putStart = currentTimeMillis();
  for(int x = 0; x < num; x++){
    char* v = test + x;
    sprintf(k, "%x", x);
    int n = width + (x % (width * 3));
    udatabase_remove(db, k, NULL);
    pthread_mutex_unlock(&db->lock); // NOTE: remove() keeps lock open.
  }
  return currentTimeMillis() - putStart;
}

/**
 * main()
 *
 * Entry point into our program. Parse the command line parameters and start
 * the database as required.
 *
 * @param argc The number of command line arguments.
 * @param argv The command line arguments.
 **/
int main(int argc, char** argv){
  /* Setup default arguments */
  char* loc = "./test.udb";
  int test = 0;
  /* Loop over arguments */
  for(int x = 0; x < argc; x++){
    /* Check we have an argument */
    if(argv[x][0] == '-'){
      /* No arguments */
      switch(argv[x][1]){
        case 'h' :
          fprintf(stderr,
            "./udatabase [OPT]\n"
            "  OPTions\n"
            "    -h  Display this help\n"
            "    -l  Set the location of the database <STR>\n"
            "    -t  Perform testing\n"
            "    -T  Perform extensive testing\n"
          );
          return 0;
        case 't' :
          test = 1;
          break;
        case 'T' :
          test = 2;
          break;
      }
      /* One argument */
      if(x + 1 < argc){
        switch(argv[x][1]){
          case 'l' :
            loc = argv[++x];
            break;
        }
      }
    }else{
      fprintf(stderr, "Unknown argument '%s'\n", argv[x]);
    }
  }
  /* Check if we should perform testing */
  if(test > 0){
    /* Create the database */
    int recordWidth = 1024;
    int dataWidth = recordWidth - 16;
    udatabase_create(loc);
    /* Initialise the database */
    struct uDB db;
    udatabase_init(&db, loc, recordWidth);
    if(db.fp == NULL){
      fprintf(stderr, "Unable to open database\n");
    }
    /* Stress test the database with creation */
    int tnum = test == 1 ? 9 : 17;
    int tvals[17] = { 4, 128, 256, 512, 1024, 2048, 4096, 8192, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216 };
    int64_t tputs[tnum];
    int64_t tgets[tnum];
    int64_t tdels[tnum];
    for(int x = 0; x < tnum; x++){
      tputs[x] = testPut(&db, dataWidth, tvals[x]);
      tgets[x] = checkPut(&db, dataWidth, tvals[x]);
      tdels[x] = delPut(&db, dataWidth, tvals[x]);
      udatabase_service(&db);
      fseek(db.fp, 0, SEEK_SET);
      assert(ftell(db.fp) == 0);
    }
    /* Close the database */
    udatabase_close(&db);
    /* Print results */
    fprintf(stderr, "Results\n");
    for(int x = 0; x < tnum; x++){
      fprintf(
        stderr,
        "  udatabase_put()\t%i recs\t%li ms\t%f ms/rec\t%f recs/sec\n",
        tvals[x],
        tputs[x],
        tputs[x] / (double)tvals[x],
        tvals[x] * (1000.0 / (double)tputs[x])
      );
    }
    for(int x = 0; x < tnum; x++){
      fprintf(
        stderr,
        "  udatabase_get()\t%i recs\t%li ms\t%f ms/rec\t%f recs/sec\n",
        tvals[x],
        tgets[x],
        tgets[x] / (double)tvals[x],
        tvals[x] * (1000.0 / (double)tgets[x])
      );
    }
    for(int x = 0; x < tnum; x++){
      fprintf(
        stderr,
        "  udatabase_remove()\t%i recs\t%li ms\t%f ms/rec\t%f recs/sec\n",
        tvals[x],
        tdels[x],
        tdels[x] / (double)tvals[x],
        tvals[x] * (1000.0 / (double)tdels[x])
      );
    }
  }
}
