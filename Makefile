FLAGS := -std=c99 -Ofast -D_XOPEN_SOURCE=500
CHECK_FLAGS := --enable=warning,style,performance,portability,information,missingInclude --std=c99

.PHONY : all

all : main.c udatabase.h
	cc $(FLAGS) -s -o udatabase main.c

debug :
	cc $(FLAGS) -g -o udatabase main.c

profile :
	cc $(FLAGS) -pg -o udatabase main.c
	./udatabase -t
	gprof -b udatabase gmon.out > analysis.out

check :
	cppcheck $(CHECK_FLAGS) --platform=unix64 udatabase.h
	cppcheck $(CHECK_FLAGS) --platform=unix32 udatabase.h

clean :
	-rm test.udb
	-rm udatabase
