# uDatabase

uDatabase is an ultra lightweight Unix key-value database implementation that
weighs in under 256 lines (including comments) in a single header file.

Features:

* *Fast* - It doesn't try to do anything too smart, it just does one single
thing well.
* *Small* - It is under 256 lines and uses minimal standard/Unix libraries. As
a result it compiles at ~15kB.
* *Threading* - Thread safe and can be accessed via multiple threads (currently
untested).
* *RAM lookup* - It minimizes time spent reading the disk by building a RAM
lookup table and expands it as required.
* *Dumb* - It doesn't do any magic.

The style is based on the [Unix
philosophy](https://en.wikipedia.org/wiki/Unix_philosophy), particularly:

> 1. Make each program do one thing well. To do a new job, build afresh rather
> than complicate old programs by adding new "features".

## Building

```
make clean # Remove any existing binary
make       # Create new binaries
```

Additionally, you may want to:

```
make debug   # Build with debug symbols
make profile # Build with profiling information and run small test
```

## Running

To run the demo, simply run:

```
./udatabase -h
```

This will show the available options for running the database.

To get started, try running a small scale test with `./udatabase -t`.

## Coding

See the [main.c](main.c) file for an example implementation. The [uDatabase
source](udatabase.h) is also well commented and easy to understand.

## Performance

The following graphs give some idea of what can be expected in terms of
performance (tested with an i7-12700H and an SSD).

![](img/1.png)

![](img/2.png)

![](img/3.png)

During tests, the largest record number (16,777,216) the index was approximately
400MB in RAM and the database file size was approximately 48GB.

The average data stored was 3 records width (1024 * 3), for 16,777,216 is 48GB.

If you need more performance, `udatabase_service()` can be ran less.
